AJS.bind("init.rte", function() {
    var baseUrl = AJS.Meta.get("context-path"),
        contentId = AJS.Meta.get("content-id"),
        endpoint = "plugins/servlet/confluence/include-page-macro/goto",
        openIncludedPage = function(macroNode) {
            var $macroNode = AJS.$(macroNode),
                location = $macroNode.attr("data-macro-default-parameter"),
                url = baseUrl + "/" + endpoint + "?location=" + location + "&contentId=" + contentId,
                windowName = (AJS.$.browser && AJS.$.browser.msie) ? "_blank" : "confluence-goto-link-include-macro-" + macroNode.id,
                win = window.open(url, windowName);

            if (win) win.focus();
        };

    AJS.Confluence.PropertyPanel.Macro.registerButtonHandler("goto-page", function(e, macroNode) {
        openIncludedPage(macroNode);
    });
});
