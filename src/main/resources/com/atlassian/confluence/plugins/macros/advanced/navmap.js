jQuery(function($) {
    $("table.navMapContainer").each(function() {
        var navmapTable = $(this);
        var navmapFields = navmapTable.next("fieldset");

        var navmapParams = {};

        navmapFields.find("input").each(function() {
            navmapParams[this.name] = this.value;
        });

        navmapTable.find(".navMapCell").each(function() {
            var navmapCell = $(this);

            navmapCell.css("width", navmapParams["navmapCellWidth"] + "px").css("height", navmapParams["navmapCellHeight"] + "px");
        });
    });
});
