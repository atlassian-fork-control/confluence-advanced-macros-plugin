package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.spring.container.ContainerManager;

public class AttachmentUpdateItem extends AbstractUpdateItem
{
    private final Attachment attachment;

    public AttachmentUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, I18NBean i18n, String iconClass)
    {
        super(searchResult, dateFormatter, i18n, iconClass);

        AnyTypeDao anyTypeDao = (AnyTypeDao) ContainerManager.getComponent("anyTypeDao");
        attachment = (Attachment) anyTypeDao.findByHandle(searchResult.getHandle());
    }

    public String getBody()
    {
/*
        // thumbnail generation is slow (adds about a second to loading of the recent update list on a local instance - disable for now
        ThumbnailInfoFactory thumbnailInfoFactory = (ThumbnailInfoFactory) ContainerManager.getComponent("thumbnailInfoFactory");
        ThumbnailInfo thumbnail = thumbnailInfoFactory.getThumbnailInfo(attachment);

        if (thumbnail != null) // if attachment can be thumbnailed
        {
            return String.format("<a href=\"%s%s\"><img border=\"0\" class=\"thumbnail\" src=\"%s\" width=\"%s\" height=\"%s\"></a>",
                    RequestCacheThreadLocal.getContextPath(),
                attachment.getDownloadPathWithoutVersion(),
                thumbnail.getThumbnailUrlPath(),
                thumbnail.getThumbnailWidth(),
                thumbnail.getThumbnailHeight());
        }
        else
        {
            return super.getBody();
        }
*/
        return super.getBody();
    }

    private String getLinkedContentTitle(Attachment attachment)
    {
        final ContentEntityObject owningContent = attachment.getContent();
        return String.format("<a href=\"%s%s\">%s</a>", RequestCacheThreadLocal.getContextPath(), owningContent.getUrlPath(), owningContent.getTitle());
    }

    ///CLOVER:OFF
    public String getDescriptionAndDateKey()
    {
        return "update.item.desc.attachment";
    }

    public String getDescriptionAndAuthorKey()
    {
        return "update.item.desc.author.attachment";
    }
    ///CLOVER:ON
}
