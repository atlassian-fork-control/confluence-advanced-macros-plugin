package com.atlassian.confluence.plugins.macros.advanced.xhtml.migration;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.macro.xhtml.MacroMigration;

import java.util.HashMap;
import java.util.Map;

public class GalleryMacroMigration implements MacroMigration
{
    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        Map<String, String> macroParameters = macroDefinition.getParameters();

        if (macroParameters.containsKey("reverseSort"))
        {
            Map<String, String> modifiedMacroParameters = new HashMap<String, String>(macroDefinition.getParameters());

            modifiedMacroParameters.remove("reverseSort");
            modifiedMacroParameters.put("reverse", "true");

            return new MacroDefinition(macroDefinition.getName(), macroDefinition.getBody(), macroDefinition.getDefaultParameterValue(), modifiedMacroParameters);
        }

        return macroDefinition;
    }
}
