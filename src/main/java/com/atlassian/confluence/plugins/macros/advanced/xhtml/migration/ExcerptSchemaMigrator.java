package com.atlassian.confluence.plugins.macros.advanced.xhtml.migration;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.ExcerptType;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

import org.apache.commons.lang3.StringUtils;

/**
 * Used to migrate include excerpts param from true/false to {@link com.atlassian.confluence.plugins.macros.advanced.xhtml.ExcerptType}
 */
public class ExcerptSchemaMigrator implements MacroMigration
{
    @Override
    public MacroDefinition migrate(MacroDefinition macro, ConversionContext context)
    {
        String oldValue = macro.getParameter("excerpt");

        if (StringUtils.isNotBlank(oldValue))
        {
            ExcerptType excerptParam = ExcerptType.fromOldValue(oldValue);
            macro.setParameter("excerptType", excerptParam.getValue());
            macro.setTypedParameter("excerptType", excerptParam);
        }

        macro.setSchemaVersion(macro.getSchemaVersion() + 1);

        return macro;
    }

}
