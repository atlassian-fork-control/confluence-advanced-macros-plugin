package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.params.ParameterException;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.macro.query.InclusionCriteria;
import com.atlassian.confluence.macro.query.params.AuthorParameter;
import com.atlassian.confluence.macro.query.params.ContentTypeParameter;
import com.atlassian.confluence.macro.query.params.LabelParameter;
import com.atlassian.confluence.macro.query.params.SpaceKeyParameter;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.renderer.v2.macro.MacroException;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringEscapeUtils;

import java.util.Map;
import java.util.Set;

import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.CompositeQueryExpression.BooleanOperator.AND;

/**
 * Converts legacy ContentFilteringMacro parameters to a CQL string.
 */
class LegacyParameterConverter
{
    private static final String OPERATOR = "operator";
    private static final String OPERATOR_AND = "AND";

    private final I18NBean i18n;
    private final SpaceKeyParameter spaceKeyParam;
    private final LabelParameter labelParam;
    private final AuthorParameter authorParam;
    private final ContentTypeParameter contentTypeParam;

    LegacyParameterConverter(I18NBean i18n, SpaceKeyParameter spaceKeyParam, LabelParameter labelParam, AuthorParameter authorParam, ContentTypeParameter contentTypeParam)
    {
        this.i18n = i18n;
        this.spaceKeyParam = spaceKeyParam;
        this.labelParam = labelParam;
        this.authorParam = authorParam;
        this.contentTypeParam = contentTypeParam;
    }

    public String buildQueryStringFromLegacyParameters(Map<String, String> parameters, MacroExecutionContext ctx) throws MacroException
    {
        // FYI - this is similar logic to generateCqlMacroParamValue() in cql-component.js.
        CompositeQueryExpression.Builder builder = CompositeQueryExpression.builder(AND);

        builder.add(getLabelExpression(parameters, ctx));
        builder.add(getCreatorExpression(ctx));
        builder.add(getTypeExpression(ctx));
        builder.add(getSpaceExpression(ctx));

        return builder.build().toQueryString();
    }

    // Package-level for testing
    QueryExpression getSpaceExpression(MacroExecutionContext ctx)
            throws ParameterException
    {
        // Special case - spaceKeyParam.findValue tries to resolve @self to space from execution context,
        // but we convert it to a CQL function. We can work around this by setting the spaceKey in the context to
        // "currentSpace()", which is safe because
        //   1. this key will be only used as a replacement for @self, and
        //   2. this is not a valid space key so nothing else could be using it.
        // It feels more than a bit hacky but allows us to continue delegating to the SpaceKeyParameter... for now.
        PageContext pageContext = new PageContext("currentSpace()");
        MacroExecutionContext macroExecutionContext = new MacroExecutionContext(ctx.getParams(), null, pageContext);

        BooleanQueryFactory spaceKeyQuery = spaceKeyParam.findValue(macroExecutionContext);
        if (spaceKeyQuery == null)
            return null;

        BooleanQuery booleanQuery = spaceKeyQuery.toBooleanQuery();
        return new BooleanQueryConverter(i18n).convertToExpression(booleanQuery);
    }

    private QueryExpression getTypeExpression(MacroExecutionContext ctx) throws MacroException
    {
        try
        {
            BooleanQueryFactory contentTypeQuery = contentTypeParam.findValue(ctx);
            if (contentTypeQuery != null)
            {
                return new BooleanQueryConverter(i18n).convertToExpression(contentTypeQuery.toBooleanQuery());
            }
        }
        catch (ParameterException pe)
        {
            throw new MacroException(
                    i18n.getText(
                            "contentbylabel.error.parse-types-param",
                            new String[]{StringEscapeUtils.escapeHtml(pe.getMessage())}
                    ),
                    pe
            );
        }
        return null;
    }

    // Package-level for unit testing
    QueryExpression getLabelExpression(Map<String, String> parameters, MacroExecutionContext ctx) throws MacroException
    {
        if (OPERATOR_AND.equalsIgnoreCase(parameters.get(OPERATOR)))
        {
            labelParam.setDefaultInclusionCriteria(InclusionCriteria.ALL);
        }

        BooleanQueryFactory queryFactory = labelParam.findValue(ctx);
        if (queryFactory == null)
        {
            // This is the contentbylabel macro, after all. Not optional!
            throw new MacroException(
                    i18n.getText("contentbylabel.error.label-parameter-required")
            );
        }

        BooleanQuery query = queryFactory.toBooleanQuery();
        return new BooleanQueryConverter(i18n).convertToExpression(query);
    }

    QueryExpression getCreatorExpression(MacroExecutionContext ctx) throws ParameterException
    {
        // parse author parameter
        Set<String> authors = authorParam.findValue(ctx);
        if (authors.isEmpty())
            return null;

        return SimpleQueryExpression.of("creator", Lists.newArrayList(authors));
    }
}
