package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.LinkRenderer;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.core.PageContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.macro.ContentFilteringMacro;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.macros.advanced.analytics.ChildrenMacroMetrics;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ContentComparatorFactory;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.MacroException;

import com.opensymphony.util.TextUtils;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * A minimal impact re-implementation of the v2 PageChildrenMacro which better understands ConversionContext with the
 * purpose of rendering links correctly on export (CONFDEV-1285).
 * <p>
 * This is really only a starting point for a full on conversion to an XHTML version of this macro.
 */
public class ChildrenMacro extends ContentFilteringMacro implements Macro
{
    private PageManager pageManager;
    private SpaceManager spaceManager;
    private LinkRenderer viewLinkRenderer;
    private PermissionManager permissionManager;
    private ContentPermissionManager contentPermissionManager;
    private ConfluenceActionSupport confluenceActionSupport; /* For i18n */
    private WebResourceManager webResourceManager;
    private EventPublisher eventPublisher;
    private AdvancedMacrosExcerpter advancedMacrosExcerpter;

    private Comparator<Page> sort;

    public boolean isInline()
    {
        return getOutputType() == OutputType.INLINE;
    }

    public boolean hasBody()
    {
        return getBodyType() != Macro.BodyType.NONE;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    /**
     * This XHTML execute method is where all the work is actually done. It's called by the {@link
     * #execute(MacroExecutionContext)} method.
     */
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        final ChildrenMacroMetrics.Builder metrics = ChildrenMacroMetrics.builder();
        final Helper helper = new Helper(metrics, context);

        webResourceManager.requireResource("confluence.macros.advanced:children-resource");

        String pageTitle = parameters.get("page");
        PageContext pageContext = context != null && context.getPageContext() != null ? context.getPageContext() : new PageContext();

        List<Page> children;
        try
        {
            if (pageTitle != null && (pageTitle.equals("/") || pageTitle.endsWith(":")))
            {
                children = helper.getRootPagesForSpace(pageTitle);
            }
            else
            {
                children = helper.getChildrenFromPage(pageTitle);
            }
        }
        catch (IllegalArgumentException e)
        {
            return RenderUtils.blockError(getConfluenceActionSupport().getText("children.error.unable-to-render"), e.getMessage());
        }

        int depth = 1;

        if ("true".equalsIgnoreCase(parameters.get("all")) || "all".equalsIgnoreCase(parameters.get("depth")))
        {
            depth = 0;
        }

        if (parameters.get("depth") != null && !"all".equalsIgnoreCase(parameters.get("depth")))
        {
            try
            {
                depth = Integer.parseInt(parameters.get("depth"));
            }
            catch (NumberFormatException e)
            {
                return RenderUtils.blockError(
                        getConfluenceActionSupport().getText("children.error.unable-to-render"),
                        getConfluenceActionSupport().getText(
                                "children.error.invalid-depth",
                                new String[] { StringEscapeUtils.escapeHtml(parameters.get("depth")) })
                );
            }
        }

        // configure the sorting alg.
        sort = configureComparator(parameters);

        // limit the number of results at the root level. but first, we need to sort to ensure
        // the correct results are displayed.
        if (sort != null)
        {
            Collections.sort(children, sort);
        }

        if (TextUtils.stringSet(parameters.get("first")))
        {
            try
            {
                int first = Integer.parseInt(parameters.get("first"));
                if (first > 0 && (first < children.size()))
                {
                    children = children.subList(0, first);
                }
            }
            catch (NumberFormatException e)
            {
                // noop
            }
        }

        ExcerptType excerptType = ExcerptType.fromString(parameters.get("excerptType"));
        final String output = helper.render(parameters, children, excerptType, depth);

        eventPublisher.publish(metrics.build());

        return output;
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public String execute(MacroExecutionContext ctx) throws MacroException
    {
        try
        {
            return execute(ctx.getParams(), ctx.getBody(), new DefaultConversionContext(ctx.getPageContext()));
        }
        catch (MacroExecutionException ex)
        {
            throw new MacroException(ex.getCause() != null ? ex.getCause() : ex);
        }
    }

    private Comparator<Page> configureComparator(Map<String, String> parameters)
    {
        String sortType = (String) parameters.get("sort");
        boolean reverse = Boolean.valueOf((String) parameters.get("reverse"));
        return ContentComparatorFactory.getComparator(sortType, reverse);
    }

    private class Helper
    {
        private final ChildrenMacroMetrics.Builder metrics;
        private final ConversionContext conversionContext;
        private final PageContext pageContext;

        Helper(final ChildrenMacroMetrics.Builder metrics, final ConversionContext conversionContext)
        {
            this.metrics = metrics;
            this.conversionContext = conversionContext;
            this.pageContext = conversionContext != null && conversionContext.getPageContext() != null ? conversionContext.getPageContext() : new PageContext();
        }

        private String render(final Map<String, String> parameters, final List<Page> children, final ExcerptType excerptType, final int depth)
                throws MacroExecutionException
        {
            metrics.renderOptions(excerptType, depth);
            if (TextUtils.stringSet(parameters.get("style")))
            {
                String style = parameters.get("style");
                if (style.matches("h[1-6]"))
                {
                    return printChildrenUnderHeadings(children, depth, excerptType, style.charAt(1));
                }
                else
                {
                    return RenderUtils.blockError(
                            getConfluenceActionSupport().getText("children.error.unable-to-render"),
                            getConfluenceActionSupport().getText(
                                    "children.error.unknown-style",
                                    new String[] { StringEscapeUtils.escapeHtml(style) })
                    );
                }

            }

            return printChildren(children, depth, excerptType);
        }

        private List<Page> getRootPagesForSpace(String pageTitle)
        {
            Space space = getSpace(pageTitle);

            final List topLevelPages = pageManager.getTopLevelPages(space);
            List<Page> pages = filterPermittedEntities(topLevelPages);

            if (pageContext.getEntity() != null)
            {
                removeThisPageFromList(pages, pageContext.getEntity());
            }

            return pages;
        }

        private Space getSpace(final String pageTitle)
        {
            Space space = null;

            if ("/".equals(pageTitle))
            {
                space = getCurrentSpace();
            }
            else if (pageTitle.length() > 1 && pageTitle.endsWith(":"))
            {
                space = spaceManager.getSpace(pageTitle.substring(0, pageTitle.length() - 1));
            }

            if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, space))
            {
                throw new IllegalArgumentException(
                        getConfluenceActionSupport().getText(
                                "children.error.space-does-not-exists",
                                new String[] { StringEscapeUtils.escapeHtml(pageTitle) })
                );
            }
            return space;
        }

        private List filterPermittedEntities(final List topLevelPages)
        {
            metrics.filterPermittedEntitiesStart(topLevelPages.size());
            final List permittedEntities = permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, topLevelPages);
            metrics.filterPermittedEntitiesFinish();
            return permittedEntities;
        }

        private Space getCurrentSpace()
        {
            Space space;
            ContentEntityObject obj = pageContext.getEntity();
            if (obj instanceof PageContentEntityObject)
            {
                obj = ((PageContentEntityObject) obj).getPage();
            }

            if (obj instanceof SpaceContentEntityObject)
            {
                space = ((SpaceContentEntityObject) obj).getSpace();
            }
            else
            {
                throw new IllegalArgumentException(getConfluenceActionSupport().getText("children.error.content-not-belong-to-space"));
            }
            return space;
        }

        private void removeThisPageFromList(List<Page> pages, ContentEntityObject thisPage)
        {
            for (Iterator<Page> it = pages.iterator(); it.hasNext(); )
            {
                Page page = (Page) it.next();
                if (page.getId() == thisPage.getId())
                {
                    it.remove();
                }
            }
        }

        private List<Page> getChildrenFromPage(String pageTitle)
        {
            List<Page> children;
            ContentEntityObject target = getPage(pageContext, pageTitle);

            if (target == null)
            {
                if (TextUtils.stringSet(pageTitle))
                {
                    throw new IllegalArgumentException(getConfluenceActionSupport().getText(
                            "children.error.page-not-found",
                            new String[] { StringEscapeUtils.escapeHtml(pageTitle) }));
                }
                else
                {
                    throw new IllegalArgumentException(getConfluenceActionSupport().getText("children.error.macro-works-on-only-pages"));
                }
            }

            if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, target))
            {
                throw new IllegalArgumentException(getConfluenceActionSupport().getText(
                        "children.error.page-not-found",
                        new String[] { StringEscapeUtils.escapeHtml(pageTitle) }));
            }

            if (!(target instanceof Page))
            {
                throw new IllegalArgumentException(
                        getConfluenceActionSupport().getText(
                                "children.error.can-only-find-children-of-a-page",
                                new String[] { target.getType() })
                );
            }

            return getPermittedChildren((Page) target);
        }


        private ContentEntityObject getPage(PageContext context, String pageTitleToRetrieve)
        {
            if (!TextUtils.stringSet(pageTitleToRetrieve))
            {
                return getCurrentPage(context);
            }

            String spaceKey = context.getSpaceKey();
            String pageTitle = pageTitleToRetrieve;

            int colonIndex = pageTitleToRetrieve.indexOf(":");
            if (colonIndex != -1 && colonIndex != pageTitleToRetrieve.length() - 1)
            {
                spaceKey = pageTitleToRetrieve.substring(0, colonIndex);
                pageTitle = pageTitleToRetrieve.substring(colonIndex + 1);
            }

            return pageManager.getPage(spaceKey, pageTitle);
        }

        private ContentEntityObject getCurrentPage(PageContext context)
        {
            ContentEntityObject entity = ContentIncludeStack.peek();
            if (entity instanceof Page)
            {
                return entity;
            }

            return context.getEntity();
        }

        private String printChildren(List<Page> pages, int depth, ExcerptType excerptType)
                throws MacroExecutionException
        {
            StringBuilder buf = new StringBuilder();
            printChildren(pages, buf, depth, excerptType);
            return buf.toString();
        }

        private String printChildrenUnderHeadings(List<Page> children, int depth, ExcerptType excerptType, char headerType)
                throws MacroExecutionException
        {

            StringBuilder buffer = new StringBuilder();

            for (Page child : children)
            {
                buffer.append("<h").append(headerType).append(">");
                buffer.append(makePageLink(child));
                buffer.append("</h").append(headerType).append(">\n");

                buffer.append(renderExcerpt(child, excerptType, "<p>", "</p>\n"));

                if (depth != 1)
                {
                    printChildren(getPermittedChildren(child), buffer, depth - 1, excerptType);
                }
            }

            return buffer.toString();
        }

        private void printChildren(List<Page> children, StringBuilder buffer, int depth, ExcerptType excerptType)
                throws MacroExecutionException
        {
            if (children.size() > 0)
            {
                if (sort != null)
                {
                    Collections.sort(children, sort);
                }

                buffer.append("<ul class='childpages-macro'>");
                for (Page child : children)
                {
                    buffer.append("<li>").append(makePageLink(child));

                    buffer.append(renderExcerpt(child, excerptType, " &mdash; <span class=\"smalltext\">", "</span>"));

                    if (depth != 1)
                    {
                        printChildren(getPermittedChildren(child), buffer, depth - 1, excerptType);
                    }
                    buffer.append("</li>");
                }
                buffer.append("</ul>");
            }
        }

        private String renderExcerpt(Page child, ExcerptType excerptType, String legacyWrapperStart, String legacyWrapperEnd)
        {
            metrics.excerptSummariseStart();
            final String summary = advancedMacrosExcerpter.createExcerpt(child, excerptType, conversionContext, legacyWrapperStart, legacyWrapperEnd);
            metrics.excerptSummariseFinish();
            return summary;
        }

        private List<Page> getPermittedChildren(final Page child)
        {
            metrics.permittedChildrenFetchStart();
            final List<Page> children = contentPermissionManager.getPermittedChildren(child, AuthenticatedUserThreadLocal.getUser());
            metrics.permittedChildrenFetchFinish(children.size());
            return children;
        }

        private String makePageLink(Page child)
                throws MacroExecutionException
        {
            try
            {
                metrics.renderPageLinkStart();
                return viewLinkRenderer.render(child, conversionContext);
            }
            catch (XhtmlException e)
            {
                throw new MacroExecutionException(e);
            }
            finally
            {
                metrics.renderPageLinkFinish();
            }
        }
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setContentPermissionManager(ContentPermissionManager contentPermissionManager)
    {
        this.contentPermissionManager = contentPermissionManager;
    }

    public void setAdvancedMacrosExcerpter(AdvancedMacrosExcerpter advancedMacrosExcerpter)
    {
        this.advancedMacrosExcerpter = advancedMacrosExcerpter;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    protected ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
        {
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        }
        return confluenceActionSupport;
    }

    public void setViewLinkRenderer(LinkRenderer viewLinkRenderer)
    {
        this.viewLinkRenderer = viewLinkRenderer;
    }

    public void setEventPublisher(final EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }
}
