package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

/**
 * Represents a simple or composite query Expression.
 */
public interface QueryExpression
{
    /**
     * Serialises this expression to a query string.
     * @return query string
     */
    String toQueryString();
}
