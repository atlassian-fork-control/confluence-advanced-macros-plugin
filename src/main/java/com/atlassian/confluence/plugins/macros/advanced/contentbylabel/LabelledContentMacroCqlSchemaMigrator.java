package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.query.params.AuthorParameter;
import com.atlassian.confluence.macro.query.params.ContentTypeParameter;
import com.atlassian.confluence.macro.query.params.LabelParameter;
import com.atlassian.confluence.macro.query.params.SpaceKeyParameter;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Migrates {@link LabelledContentMacro} parameters from legacy format to CQL format.
 */
public class LabelledContentMacroCqlSchemaMigrator implements MacroMigration
{
    private static final Logger log = LoggerFactory.getLogger(LabelledContentMacroCqlSchemaMigrator.class);

    private I18NBeanFactory i18NBeanFactory;

    @Override
    public MacroDefinition migrate(MacroDefinition macro, ConversionContext context)
    {
        String cql = getCql(macro, context);

        macro.setParameter("cql", cql);
        macro.setTypedParameter("cql", cql);
        log.debug("CQL parameter set to '{}'", cql);

        macro.setSchemaVersion(2);

        return macro;
    }

    private String getCql(MacroDefinition macro, ConversionContext context)
    {
        LabelParameter labelParam = new LabelParameter();
        labelParam.setValidate(false);
        labelParam.addParameterAlias(""); // the unnamed parameter - very legacy.

        ContentTypeParameter contentTypeParam = new ContentTypeParameter();

        // The space param must have the same settings as the old one in LabelledContentMacro.
        SpaceKeyParameter spaceKeyParam = new SpaceKeyParameter();
        spaceKeyParam.addParameterAlias("key");
        spaceKeyParam.setDefaultValue("@all");

        AuthorParameter authorParam = new AuthorParameter();

        LegacyParameterConverter converter = new LegacyParameterConverter(getI18nBeanFactory().getI18NBean(),
                spaceKeyParam, labelParam, authorParam, contentTypeParam);
        PageContext pageContext = context.getPageContext();
        Map<String, String> parameters = macro.getParameters();
        MacroExecutionContext macroContext = new MacroExecutionContext(parameters, null, pageContext);
        try
        {
            return converter.buildQueryStringFromLegacyParameters(parameters, macroContext);
        }
        catch (MacroException e)
        {
            // Will only happen if no label param saved - this will be pointed out in the e message.
            log.debug("Exception thrown when migrating parameters to CQL", e);
            throw new IllegalArgumentException("Unable to migrate contentbylabel parameters", e);
        }
    }

    private I18NBeanFactory getI18nBeanFactory()
    {
        if (i18NBeanFactory == null)
        {
            i18NBeanFactory = ContainerManager.getComponent("i18NBeanFactory", I18NBeanFactory.class);
        }
        return i18NBeanFactory;
    }
}
