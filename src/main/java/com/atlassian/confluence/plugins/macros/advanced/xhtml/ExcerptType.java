package com.atlassian.confluence.plugins.macros.advanced.xhtml;

/**
 * Used to allow different rendering modes for the excerpts.
 *
 * The values are used to do the conversion from the UI.
 */
public enum ExcerptType
{
    /**
     * Don't render excerpts, equivalent to previous include excerpts = false.
     *
     * Shown to the user as "none"
     */
    NONE("none"),
    /**
     * Render excerpts as before RENDERED mode was introduced, equivalent to previous include excerpts = true.
     *
     * Shown to the user as "simple"
     */
    LEGACY("simple"),
    /**
     * Render full excerpts with rich content
     *
     * Shown to the user as "rich content"
     */
    RENDERED("rich content");

    private final String value;

    private ExcerptType(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    public static ExcerptType fromString(String value)
    {
        for (ExcerptType type : ExcerptType.values())
        {
            if (type.getValue().equalsIgnoreCase(value))
            {
                return type;
            }
        }

        return NONE;
    }

    public static ExcerptType fromOldValue(String oldValue)
    {
        return Boolean.valueOf(oldValue) ? LEGACY : NONE;
    }
}