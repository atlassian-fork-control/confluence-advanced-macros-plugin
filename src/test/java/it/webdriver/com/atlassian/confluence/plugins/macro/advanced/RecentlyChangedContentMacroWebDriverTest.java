package it.webdriver.com.atlassian.confluence.plugins.macro.advanced;

import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.Iterables;
import org.hamcrest.BaseMatcher;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.collection.IsIterableWithSize;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Test;

public class RecentlyChangedContentMacroWebDriverTest extends AbstractWebDriverTest
{
    @Test
    public void testGetMoreResultsActivityStream() throws Exception
    {
        rpc.logIn(User.TEST);

        int total = 15;
        long[] pageIds = new long[total];
        for (int i = total; i >= 1; i--) // create in reverse older so lower numbered pages are most recent
            pageIds[i - 1] = Long.valueOf(rpc.createPage(Space.TEST.getKey(), "Test Page " + i, "", -1).get("id"));

        rpc.logIn(User.ADMIN);
        rpc.flushIndexQueue();

        ViewUserProfileActivityStreamPage viewUserProfilePage = product.login(User.TEST, ViewUserProfileActivityStreamPage.class);

        ActivityStreamElement activityStream = viewUserProfilePage.getActivityStream();

        PageElement moreLink = activityStream.getMoreLink();
        Poller.waitUntilTrue("Missing title input field", moreLink.timed().isPresent());

        moreLink.click();

        Poller.waitUntil(activityStream.getStreamTitles(), IsIterableWithSize.<String>iterableWithSize(total));
        Poller.waitUntil(activityStream.getStreamTitles(), new BaseMatcher<Iterable<String>>()
        {
            @Override
            public boolean matches(Object item)
            {
                Iterable<String> iterable = (Iterable<String>) item;

                return Iterables.contains(iterable, "Test Page 11");
            }

            @Override
            public void describeTo(Description description)
            {
            }
        });

        /**
         * Now test the scenario when:
         *
         * 1. Fred loads activity stream
         * 2. Another user deletes a page
         * 3. Fred clicks the "More" link. He should still see the deleted page (as he is operating off an older version of the index)
         */
        viewUserProfilePage = product.visit(ViewUserProfileActivityStreamPage.class);
        activityStream = viewUserProfilePage.getActivityStream();

        rpc.removePage(pageIds[10]); // remove 11th page
        rpc.purgeFromTrash(Space.TEST, pageIds[10]);
        rpc.flushIndexQueue();

        activityStream.getMoreLink().click();

        Poller.waitUntil(activityStream.getStreamTitles(), IsCollectionContaining.hasItems("Test Page 11"));

        /**
         * Fred refreshes the activity stream.
         *
         * He should no longer see the deleted page in the stream (the ability to page over an older version of the index
         * only for a short period of time and is tied to a particular instance of a "more link").
         */
        viewUserProfilePage = product.visit(ViewUserProfileActivityStreamPage.class);
        activityStream = viewUserProfilePage.getActivityStream();
        activityStream.getMoreLink().click();
        Poller.waitUntil(activityStream.getStreamTitles(), CoreMatchers.not(IsCollectionContaining.hasItems("Test Page 11")));
    }
}
