package it.com.atlassian.confluence.plugins.macros.advanced.children;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.ExcerptType;

import org.junit.Test;

import it.com.atlassian.confluence.plugins.macros.advanced.BaseExcerptsTestCase;

import static net.sourceforge.jwebunit.junit.JWebUnit.assertTextNotPresent;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertTextPresent;
import static net.sourceforge.jwebunit.junit.JWebUnit.getPageSource;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

public class ChildrenMacroExcerptsTestCase extends BaseExcerptsTestCase
{
    @Test
    public void testChildrenMacroWithLegacyExcerpt() throws Exception
    {
        excerptSetupAndBasicChecks(ExcerptType.LEGACY);
        assertTextNotPresent(GRAND_CHILD_PAGE_CONTENT);
        assertThat(getPageSource(), not(containsString(P_EXCERPT_TEXT_P)));
    }

    @Test
    public void testChildrenMacroWithRenderedExcerpt() throws Exception
    {
        excerptSetupAndBasicChecks(ExcerptType.RENDERED);
        assertTextPresent(GRAND_CHILD_PAGE_CONTENT);
        assertThat(getPageSource(), containsString(P_EXCERPT_TEXT_P));
    }

    private void excerptSetupAndBasicChecks(ExcerptType excerptType)
    {

        Page parentPage = new Page(Space.TEST, PARENT_PAGE_NAME,
                "<ac:structured-macro ac:name=\"children\" ac:schema-version=\"2\">"
                        + "<ac:parameter ac:name=\"all\">true</ac:parameter>"
                        + "<ac:parameter ac:name=\"excerptType\">" + excerptType + "</ac:parameter>"
                        + "</ac:structured-macro>");
        long parentId =rpc.createPage(parentPage);

        excerptSetupAndBasicChecks(parentId);
    }
}
