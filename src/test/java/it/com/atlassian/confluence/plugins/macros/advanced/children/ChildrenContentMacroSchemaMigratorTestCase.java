package it.com.atlassian.confluence.plugins.macros.advanced.children;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.it.AcceptanceTestHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.jsoup.JSoupTester;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.rest.api.model.ExpansionsParser;

import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TestName;

import static com.atlassian.confluence.api.model.content.ContentRepresentation.STORAGE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests that version 1 children macro instances are upgraded to version 2
 */
public class ChildrenContentMacroSchemaMigratorTestCase
{
    private AcceptanceTestHelper helper = AcceptanceTestHelper.make();
    private TestName testName = new TestName();
    private ConfluenceRpc rpc;

    @Before
    public void setUp() throws Exception
    {
        helper.setUp(getClass(), testName);
        rpc = helper.getRpc();
        rpc.logIn(User.ADMIN);
    }

    @Test
    public void testMigrationDuringStorage() throws Exception
    {
        Page page = new Page(Space.TEST, "ChildrenContentMacro Migration",
                "<ac:structured-macro ac:name=\"children\">" +
                        "<ac:parameter ac:name=\"excerpt\">true</ac:parameter>" +
                "</ac:structured-macro>");
        Page created = rpc.content.createPage(page, STORAGE);

        // Getting the page in storage format should get the migrated, stored format.
        Content content = rpc.content.get(created.getContentId(), ExpansionsParser.parseSingle("body.storage"));
        String body = content.getBody().get(STORAGE).getValue();
        JSoupTester soup = new JSoupTester(body);
        Element macroElement = soup.global().byAttribute("ac:name", "children").first();

        assertThat(macroElement.attr("ac:schema-version"), is("2"));
        assertThat(macroElement.getElementsByAttributeValue("ac:name", "excerptType").text(), is("simple"));
    }
}
