package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.query.params.AuthorParameter;
import com.atlassian.confluence.macro.query.params.ContentTypeParameter;
import com.atlassian.confluence.macro.query.params.LabelParameter;
import com.atlassian.confluence.macro.query.params.SpaceKeyParameter;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.google.common.collect.Maps;
import org.mockito.Mock;

import java.util.Map;

import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.CompositeQueryExpression.BooleanOperator.AND;
import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.SimpleQueryExpression.InclusionOperator.EXCLUDES;
import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.SimpleQueryExpression.InclusionOperator.INCLUDES;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

// This test needs to extend AbstractTestCase for Parameter classes' setup.
public class LegacyParameterConverterTest extends AbstractTestCase
{
    private LegacyParameterConverter converter;

    @Mock private I18NBean i18n;
    private Map<String, String> parameters;
    private MacroExecutionContext ctx;

    private SpaceKeyParameter spaceKeyParam;

    public void setUp() throws Exception
    {
        super.setUp();

        LabelParameter labelParam = new LabelParameter();
        ContentTypeParameter contentTypeParam = new ContentTypeParameter();
        spaceKeyParam = new SpaceKeyParameter();
        AuthorParameter authorParam = new AuthorParameter();

        parameters = Maps.newHashMap();
        parameters.put("label", "foo");   // for now, CQL build always fails if no label, so add one.
        PageContext pageContext = mock(PageContext.class);
        ctx = new MacroExecutionContext(parameters, null, pageContext);

        converter = new LegacyParameterConverter(i18n, spaceKeyParam, labelParam, authorParam, contentTypeParam);
    }

    public void testGlobalSpaceCategory() throws Exception
    {
        parameters.put("space", "@global");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space.type", INCLUDES, "global");
    }

    public void testPersonalSpaceCategory() throws Exception
    {
        parameters.put("space", "@personal");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space.type", INCLUDES, "personal");
    }

    public void testCurrentSpace() throws Exception
    {
        parameters.put("space", "@self");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", INCLUDES, "currentSpace()");
    }

    public void testNotCurrentSpace() throws Exception
    {
        parameters.put("space", "-@self");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", EXCLUDES, "currentSpace()");
    }

    public void testFavouriteSpaces() throws Exception
    {
        parameters.put("space", "@favourite");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space.type", INCLUDES, "favourite");

        parameters.put("space", "@favorite");
        expression = converter.getSpaceExpression(ctx);
        assertExpression(expression, "space.type", INCLUDES, "favourite");
    }

    public void testFavouriteSpacesButNotCurrentSpace() throws Exception
    {
        parameters.put("space", "@favourite,-@self");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertThat(expression, instanceOf(CompositeQueryExpression.class));
        CompositeQueryExpression composite = (CompositeQueryExpression) expression;

        assertThat(composite.size(), is(2));
        assertThat(composite.getOperator(), is(AND));

        assertExpression(composite.get(0), "space.type", INCLUDES, "favourite");
        assertExpression(composite.get(1), "space", EXCLUDES, "currentSpace()");
    }

    public void testAllSpaceCategory() throws Exception
    {
        parameters.put("space", "@all");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertThat(expression, instanceOf(EmptyQueryExpression.class));

        parameters.put("space", "*");
        expression = converter.getSpaceExpression(ctx);
        assertThat(expression, instanceOf(EmptyQueryExpression.class));

        parameters.remove("space");
        expression = converter.getSpaceExpression(ctx);
        assertThat(expression, nullValue());
    }

    public void testSingleSpace() throws Exception
    {
        parameters.put("space", "foo");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", INCLUDES, "foo");
    }

    public void testMultipleSpaces() throws Exception
    {
        parameters.put("space", "foo,bar");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", INCLUDES, "foo", "bar");
    }

    public void testMultipleSpacesWithWhitespaceToken() throws Exception
    {
        parameters.put("space", "foo bar");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", INCLUDES, "foo", "bar");
    }

    public void testSpacesParameterName() throws Exception
    {
        parameters.put("spaces", "foo");        // spaces as the parameter name should still work.

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", INCLUDES, "foo");
    }

    public void testKeyParameterAlias() throws Exception
    {
        spaceKeyParam.addParameterAlias("key");  // this is what the LabelledContentMacroDoes
        parameters.put("key", "foo");        // key as the space parameter alias should still work.

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", INCLUDES, "foo");
    }

    public void testMultipleSpaceCategories() throws Exception
    {
        parameters.put("space", "@global,@favourite");

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space.type", INCLUDES, "global", "favourite");
    }

    public void testExcludedSpace() throws Exception
    {
        parameters.put("space", "-foo");        // any space except for "foo"

        QueryExpression expression = converter.getSpaceExpression(ctx);

        assertExpression(expression, "space", EXCLUDES, "foo");
    }

    public void testSingleCreator() throws Exception
    {
        parameters.put("author", "testuser");

        QueryExpression expression = converter.getCreatorExpression(ctx);

        assertSingleExpression(expression, "creator", INCLUDES, "testuser");
    }


    public void testSingleLabel() throws Exception
    {
        parameters.put("label", "foo");

        QueryExpression expression = converter.getLabelExpression(parameters, ctx);

        assertSingleExpression(expression, "label", INCLUDES, "foo");
    }

    public void testSingleLabelWithWhitespace() throws Exception
    {
        parameters.put("label", " foo ");

        QueryExpression expression = converter.getLabelExpression(parameters, ctx);

        assertSingleExpression(expression, "label", INCLUDES, "foo");
    }

    public void testMultipleLabelsWithCommaDelimiter() throws Exception
    {
        parameters.put("label", "foo,bar");

        QueryExpression expression = converter.getLabelExpression(parameters, ctx);

        assertSingleExpression(expression, "label", INCLUDES, "foo", "bar");
    }

    public void testMultipleLabelsWithCommaDelimiterAndWhitespace() throws Exception
    {
        parameters.put("label", "foo, bar");

        QueryExpression expression = converter.getLabelExpression(parameters, ctx);

        assertSingleExpression(expression, "label", INCLUDES, "foo", "bar");
    }

    public void testMultipleLabelsWithSpaceDelimiter() throws Exception
    {
        parameters.put("label", "foo bar");

        QueryExpression expression = converter.getLabelExpression(parameters, ctx);

        assertSingleExpression(expression, "label", INCLUDES, "foo", "bar");
    }

    public void testMultipleLabelsWithAndOperator() throws Exception
    {
        parameters.put("label", "foo,bar");
        parameters.put("operator", "and");

        CompositeQueryExpression expression = (CompositeQueryExpression) converter.getLabelExpression(parameters, ctx);

        assertThat(expression.getOperator(), is(AND));
        assertExpression(getSimple(expression, 0), "label", INCLUDES, "foo");
        assertExpression(getSimple(expression, 1), "label", INCLUDES, "bar");
    }

    public void testMandatoryLabels() throws Exception
    {
        parameters.put("label", "+foo +bar");

        CompositeQueryExpression expression = (CompositeQueryExpression) converter.getLabelExpression(parameters, ctx);

        assertThat(expression.getOperator(), is(AND));
        assertExpression(getSimple(expression, 0), "label", INCLUDES, "foo");
        assertExpression(getSimple(expression, 1), "label", INCLUDES, "bar");
    }

    public void testCreator() throws Exception
    {
        parameters.put("author", "dtaylor");

        QueryExpression expression = converter.getCreatorExpression(ctx);

        assertExpression(expression, "creator", INCLUDES, "dtaylor");
    }

    private void assertExpression(QueryExpression expression, String key, SimpleQueryExpression.InclusionOperator inclusionOperator, String... values)
    {
        SimpleQueryExpression simple = (SimpleQueryExpression) expression;

        assertThat(simple.getKey(), is(key));
        assertThat(simple.getInclusionOperator(), is(inclusionOperator));
        assertThat(simple.getValues(), hasItems(values));
    }

    private void assertSingleExpression(QueryExpression expression, String key, SimpleQueryExpression.InclusionOperator inclusionOperator, String... values)
    {
        assertThat(expression, instanceOf(SimpleQueryExpression.class));
        assertExpression(expression, key, inclusionOperator, values);
    }
    
    /**
     * Attempts to return the expression at index i as a simple expression.
     */
    public SimpleQueryExpression getSimple(CompositeQueryExpression expression, int i)
    {
        return (SimpleQueryExpression) expression.get(i);
    }
}
