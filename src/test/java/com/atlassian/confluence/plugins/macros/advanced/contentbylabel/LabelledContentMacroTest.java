package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.pagination.PageRequest;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.pagination.PageResponseImpl;
import com.atlassian.confluence.api.model.search.SearchContext;
import com.atlassian.confluence.api.service.search.CQLSearchService;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.plugins.macros.advanced.analytics.LabelledContentMacroMetrics;
import com.atlassian.confluence.renderer.PageContext;
import com.google.common.collect.Maps;
import org.hamcrest.Matchers;
import org.junit.Before;

import java.util.Map;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LabelledContentMacroTest extends AbstractTestCase
{
    private final LabelledContentMacroMetrics.Builder metrics = LabelledContentMacroMetrics.builder();

    private LabelledContentMacro macro;
    private Map<String, String> parameters;
    private MacroExecutionContext ctx;

    @Before
    public void setUp() throws Exception
    {
        super.setUp();

        macro = new LabelledContentMacro();

        CQLSearchService searchService = mock(CQLSearchService.class);
        PageResponse<Content> response = PageResponseImpl.empty(false);
        when(searchService.searchContent(anyString(), any(SearchContext.class), any(PageRequest.class))).thenReturn(response);
        macro.setSearchService(searchService);

        parameters = Maps.newHashMap();
        ctx = new MacroExecutionContext(parameters, null, new PageContext());
    }

    public void testLabelsLimitedToSpaceForSingleQuery() throws Exception
    {
        String cql = "space = foo";

        Map<String, Object> renderContext = macro.makeRenderContext(ctx, parameters, cql, metrics);

        assertThat(renderContext, hasEntry("limitLabelLinksToSpace", true));
    }

    public void testLabelsNotLimitedToSpaceForMultipleQuery() throws Exception
    {
        String cql = "space in (foo,bar)";

        Map<String, Object> renderContext = macro.makeRenderContext(ctx, parameters, cql, metrics);

        assertThat(renderContext, hasEntry("limitLabelLinksToSpace", false));
    }
}
