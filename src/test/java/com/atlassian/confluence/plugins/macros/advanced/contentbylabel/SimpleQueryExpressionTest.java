package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import org.junit.Test;

import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.SimpleQueryExpression.InclusionOperator.EXCLUDES;
import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.SimpleQueryExpression.of;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test that the SimpleQueryExpression renders the correct CQL expression string.
 */
public class SimpleQueryExpressionTest
{
    @Test
    public void testSingleValue() throws Exception
    {
        SimpleQueryExpression expression = of("key", "value");
        assertThat(expression.toQueryString(), is("key = \"value\""));
    }

    @Test
    public void testMultiValue() throws Exception
    {
        SimpleQueryExpression expression = of("key", "value1", "value2");
        assertThat(expression.toQueryString(), is("key in (\"value1\",\"value2\")"));
    }

    @Test
    public void testSingleNotValue() throws Exception
    {
        SimpleQueryExpression expression = of("key", EXCLUDES, "value");
        assertThat(expression.toQueryString(), is("key != \"value\""));
    }

    @Test
    public void testMultiNotValue() throws Exception
    {
        SimpleQueryExpression expression = of("key", EXCLUDES, "value1", "value2");
        assertThat(expression.toQueryString(), is("key not in (\"value1\",\"value2\")"));
    }

    @Test
    public void testSingleFunctionValue() throws Exception
    {
        SimpleQueryExpression expression = of("space", "currentSpace()");
        assertThat(expression.toQueryString(), is("space = currentSpace()"));
    }

    @Test
    public void testMixedFunctionValues() throws Exception
    {
        SimpleQueryExpression expression = of("space", "currentSpace()", "space1");
        assertThat(expression.toQueryString(), is("space in (currentSpace(),\"space1\")"));
    }

    @Test
    public void testEscapedValueWithQuoteWrapping() throws Exception
    {
        SimpleQueryExpression expression = of("key", "\"wrapped\"");
        assertThat(expression.toQueryString(), is("key = \"\\\"wrapped\\\"\""));
    }

    @Test
    public void testEscapedValueWithInternalQuote() throws Exception
    {
        SimpleQueryExpression expression = of("key", "craz\"y");
        assertThat(expression.toQueryString(), is("key = \"craz\\\"y\""));
    }

    @Test
    public void testValueWithInvertedComma() throws Exception
    {
        SimpleQueryExpression expression = of("key", "don't stop beliebing");
        assertThat(expression.toQueryString(), is("key = \"don't stop beliebing\""));
    }
}
