package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import org.junit.Test;

import static com.atlassian.confluence.plugins.macros.advanced.contentbylabel.CompositeQueryExpression.BooleanOperator.AND;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests CQL phrase-string generation.
 */
public class CompositeQueryExpressionTest
{
    @Test
    public void testAddSingleExpressionToString() throws Exception
    {
        SimpleQueryExpression simpleQueryExpression = SimpleQueryExpression.of("foo", "bar");
        QueryExpression expression = CompositeQueryExpression.builder(AND).add(simpleQueryExpression).build();

        assertThat(expression.toQueryString(), is(simpleQueryExpression.toQueryString()));
    }

    @Test
    public void testMultipleExpressionsToString() throws Exception
    {
        SimpleQueryExpression expression1 = SimpleQueryExpression.of("key1", "value1");
        SimpleQueryExpression expression2 = SimpleQueryExpression.of("key2", "value2");

        QueryExpression expression = CompositeQueryExpression.builder(AND).add(expression1).add(expression2).build();

        assertThat(expression.toQueryString(), is(expression1.toQueryString() + " and " + expression2.toQueryString()));
    }

    @Test
    public void testNoExpressionsToString() throws Exception
    {
        QueryExpression expression = CompositeQueryExpression.builder(AND).build();

        assertThat(expression.toQueryString(), is(""));
    }
}
