package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import com.atlassian.renderer.RenderContext;
import com.google.common.collect.Maps;
import junit.framework.TestCase;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.Mockito.when;

public class MacroPanelTest extends TestCase
{
    private String title = "Panel Title";
    private String body = "Panel Body";
    private Map<String, String> parameters = Maps.newHashMap();

    @Mock private RenderContext pageContext;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);
    }

    public void testWrap() throws Exception
    {
        String html = MacroPanel.wrap(title, body, parameters, pageContext);

        Document document = Jsoup.parseBodyFragment(html);

        // Assert that the short-call of wrap uses default classnames.
        assertEquals(1, document.getElementsByClass("panel").size());
        String panelHeader = document.getElementsByClass("panelHeader").get(0).text();
        String panelContent = document.getElementsByClass("panelContent").get(0).text();

        assertEquals(title, panelHeader);
        assertEquals(body, panelContent);
    }

    public void testWrapWithClassnameOverrides() throws Exception
    {
        String panelCSSClass = "pannle";
        String panelContentCSSClass = "pannleContent";
        String panelHeaderCSSClass = "pannleHeader";
        String html = MacroPanel.wrap(title, body, parameters, pageContext, panelCSSClass,
            panelContentCSSClass, panelHeaderCSSClass);

        Document document = Jsoup.parseBodyFragment(html);

        // Assert that the short-call of wrap uses default classnames.
        assertEquals(1, document.getElementsByClass(panelCSSClass).size());
        String panelHeader = document.getElementsByClass(panelHeaderCSSClass).get(0).text();
        String panelContent = document.getElementsByClass(panelContentCSSClass).get(0).text();

        assertEquals(title, panelHeader);
        assertEquals(body, panelContent);
    }

    public void testWysiwyg() throws Exception
    {
        when(pageContext.isRenderingForWysiwyg()).thenReturn(true);

        String html = MacroPanel.wrap(title, body, parameters, pageContext);
        Document document = Jsoup.parseBodyFragment(html);

        assertEquals("ignore", document.getElementsByClass("panelHeader").get(0).attr("wysiwyg"));
    }

    public void testWithStyleParameters() throws Exception
    {
        // We don't test the parameter validation here, just that they get passed through correctly.
        parameters.put("borderStyle", "solid");
        parameters.put("borderColor", "red");
        parameters.put("bgColor", "blue");
        parameters.put("titleBGColor", "green");
        parameters.put("borderWidth", "5px");

        String html = MacroPanel.wrap(title, body, parameters, pageContext);
        Document document = Jsoup.parseBodyFragment(html);

        String panelStyle = "background-color: blue;border-color: red;border-style: solid;border-width: 5px;";
        String panelHeaderStyle = "border-bottom-width: 5px;border-bottom-style: solid;border-bottom-color: red;background-color: green;";
        String panelContentStyle = "background-color: blue;";

        assertEquals(panelStyle, document.getElementsByClass("panel").get(0).attr("style"));
        assertEquals(panelHeaderStyle, document.getElementsByClass("panelHeader").get(0).attr("style"));
        assertEquals(panelContentStyle, document.getElementsByClass("panelContent").get(0).attr("style"));
    }

}
